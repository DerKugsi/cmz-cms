-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Server-Version: 5.6.26
-- PHP-Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `cmz`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Categories`
--

CREATE TABLE IF NOT EXISTS `Categories` (
  `catid` bigint(20) unsigned NOT NULL,
  `catname` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Categories`
--

INSERT INTO `Categories` (`catid`, `catname`) VALUES
(1, 'Probe'),
(2, 'Auftritt'),
(3, 'Treffen');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Event`
--

CREATE TABLE IF NOT EXISTS `Event` (
  `eventid` int(11) NOT NULL,
  `ename` varchar(60) NOT NULL,
  `date` datetime NOT NULL,
  `details` varchar(150) NOT NULL,
  `catid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='es gibt tabellenkommentare?';

--
-- Daten für Tabelle `Event`
--

INSERT INTO `Event` (`eventid`, `ename`, `date`, `details`, `catid`) VALUES
(1, 'Probe01', '2016-06-17 19:30:00', 'Hier koennten ihre Details stehen. lol', 1),
(2, 'Probe02', '2016-06-24 19:30:00', 'Hier koennten schon wieder ihre Details stehen.', 1),
(3, 'Sommerkonzert', '2016-07-09 20:00:00', 'Unser alljährliches Sommerkonzert 2016. Dieses Jahr in der Promenade in Zwettl', 2),
(4, 'gemuetlicher Fruehschoppen', '2016-07-10 11:30:00', '#abissitrinken', 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Participants`
--

CREATE TABLE IF NOT EXISTS `Participants` (
  `Uid` int(11) NOT NULL,
  `Eid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Participants`
--

INSERT INTO `Participants` (`Uid`, `Eid`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Roles`
--

CREATE TABLE IF NOT EXISTS `Roles` (
  `roleid` bigint(20) unsigned NOT NULL,
  `rolename` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Roles`
--

INSERT INTO `Roles` (`roleid`, `rolename`) VALUES
(1, 'Musiker'),
(2, 'Funktionaer'),
(3, 'Kapellmeister');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `userid` int(11) NOT NULL,
  `uname` varchar(25) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `instrument` varchar(20) NOT NULL,
  `birth_date` date NOT NULL,
  `roleid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `User`
--

INSERT INTO `User` (`userid`, `uname`, `pwd`, `instrument`, `birth_date`, `roleid`) VALUES
(1, 'Flo', 'flo', 'Tenorhorn', '1998-04-25', 1),
(2, 'Octo', 'octo', 'Triangel', '1998-08-05', 1),
(3, 'Herti', 'herti', 'Klarinette', '1979-09-02', 3),
(4, 'Fichtl', 'fichtl', 'Tuba', '1987-11-18', 2),
(6, 'Koppi', 'koppi', 'Schlagwerk', '1998-04-02', 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`catid`),
  ADD UNIQUE KEY `catid` (`catid`);

--
-- Indizes für die Tabelle `Event`
--
ALTER TABLE `Event`
  ADD PRIMARY KEY (`eventid`);

--
-- Indizes für die Tabelle `Participants`
--
ALTER TABLE `Participants`
  ADD PRIMARY KEY (`Uid`,`Eid`);

--
-- Indizes für die Tabelle `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`roleid`),
  ADD UNIQUE KEY `roleid` (`roleid`);

--
-- Indizes für die Tabelle `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Categories`
--
ALTER TABLE `Categories`
  MODIFY `catid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `Event`
--
ALTER TABLE `Event`
  MODIFY `eventid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `Roles`
--
ALTER TABLE `Roles`
  MODIFY `roleid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `User`
--
ALTER TABLE `User`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
