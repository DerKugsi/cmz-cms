<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="Css.css">
	       <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
	       <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
            <script>
                $(document).ready(function(){
                    $('.newevent').each(function(){
                        $(this).click(function(){
                            window.location.href="newevent.php";
                        });
                    });//END each(function)
                });//END document.ready
                
            </script>
		<title>CMZ CMS</title>
	</head>
	<body>
	

        <div>
            <h1>Musiker CMS</h1>
                
                <?php
                
                include 'db.php';
                $username = $_SESSION['user'];
                
                $stmt = $db->prepare('SELECT uname FROM User where uname = :un');
                $stmt->bindParam(":un", $username, PDO::PARAM_STR);
                $stmt->execute();
                $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($res as $row) {
                echo "Eingeloggt als: ".$row['uname']."<br><br>";
                //echo $row['roleid'];
                }

                
                $stmt2 = $db->prepare('SELECT roleid FROM User where uname = :un');
                $stmt2->bindParam(":un", $username, PDO::PARAM_STR);
                $stmt2->execute();
                $res2 = $stmt2->fetch();
                
                $curroleid = $res2['roleid'];
                
                $allevent = $db->query("select * from event");
                $resallevent = $allevent->fetchAll(PDO::FETCH_ASSOC);

                ?>
                <button class="newevent">Neuen Termin erstellen</button>
                <table id="table1">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Datum</th>
                        <th>Details</th>
                        <th>CatID</th>
                    </tr>
                    
                    <?php
                    if($curroleid >= 3){
                        ?>
                        
                    <?php
                        
                    }
                        foreach($resallevent as $row){
                    ?>
                    <tr id="row<?php echo $row['eventid'] ?>">
                        <td><?php echo $row['eventid']; ?></td>
                        <td><?php echo $row['ename']; ?></td>
                        <td><?php echo $row['date']; ?></td>
                        <td><?php echo $row['details']; ?></td>
                        <td><?php echo $row['catid']; ?></td>
                        <td>
                            <?php echo "<a>"; ?>
                            
                        </td>
                    </tr>
                    <?php } //END foreach ?>
                </table>
		</div>
        
	</body>
</html>